<?php
/**
 * Created by JetBrains PhpStorm.
 * User: dimon
 * Date: 01.09.13
 * Time: 15:28
 * To change this template use File | Settings | File Templates.
 */

class WebHelper {
    public static function addClass($class, $htmlOptions = array()){
        if(isset($htmlOptions['class']))
            $htmlOptions['class'] .= ' '.$class;
        else
            $htmlOptions['class'] = $class;

        return $htmlOptions;
    }
}