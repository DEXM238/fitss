CREATE TABLE tbl_user (
    id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    username VARCHAR(128) NOT NULL,
    password VARCHAR(128) NOT NULL,
    email VARCHAR(128) NOT NULL
);

CREATE TABLE tbl_exercise (
    id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    title VARCHAR(128) NOT NULL,
    muscle_id INTEGER NOT NULL
);

CREATE TABLE tbl_workout (
  id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
  exercise_id INTEGER NOT NULL,
  FOREIGN KEY (exercise_id) REFERENCES tbl_exercise(id)
  ON UPDATE CASCADE
  ON DELETE RESTRICT,
  user_id INTEGER NOT NULL,
  FOREIGN KEY (user_id) REFERENCES tbl_user(id)
  ON UPDATE CASCADE
  ON DELETE CASCADE,
  date TIMESTAMP
);

CREATE TABLE tbl_set(
  id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
  number INTEGER NOT NULL,
  repeats INTEGER NOT NULL,
  weight INTEGER  NOT NULL,
  workout_id INTEGER NOT NULL,
  FOREIGN KEY (workout_id) REFERENCES tbl_workout(id)
  ON UPDATE CASCADE
  ON DELETE CASCADE
);

CREATE TABLE  tbl_muscles(
  id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
  title VARCHAR(128) NOT NULL
);


INSERT INTO tbl_user (username, password, email) VALUES ('test1', 'pass1', 'test1@example.com');
INSERT INTO tbl_user (username, password, email) VALUES ('test2', 'pass2', 'test2@example.com');
