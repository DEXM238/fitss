<?php
/**
 * Created by JetBrains PhpStorm.
 * User: dimon
 * Date: 08.09.13
 * Time: 16:37
 * To change this template use File | Settings | File Templates.
 */
?>
<div class="row short-exercise">
<?php foreach($exercises as $exercise): ?>

    <article class="col-md-5">
        <h5><?php echo $exercise->title; ?></h5>

        <div class="row">
            <div class="col-md-6"><img src="http://placekitten.com/250/250" /></div>
            <div class="col-md-6">
                <p>Тип: базовое</p>
                <p>Оборудование: скамья, штанга</p>
            </div>
        </div>
        <div class="row">
            Some text
        </div>

        <div class="rating"></div>
    </article>

<?php endforeach; ?>
</div>
