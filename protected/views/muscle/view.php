<?php
/* @var $this MuscleController */
/* @var $model Muscle */

$this->breadcrumbs=array(
	'Muscles'=>array('index'),
	$model->title,
);

$this->menu=array(
	array('label'=>'List Muscle', 'url'=>array('index')),
	array('label'=>'Create Muscle', 'url'=>array('create')),
	array('label'=>'Update Muscle', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Muscle', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Muscle', 'url'=>array('admin')),
);
?>

<h1>View Muscle #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'title',
	),
)); ?>
