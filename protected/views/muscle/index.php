<?php
/* @var $this MuscleController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Muscles',
);

$this->menu=array(
	array('label'=>'Create Muscle', 'url'=>array('create')),
	array('label'=>'Manage Muscle', 'url'=>array('admin')),
);
?>

<h1>Muscles</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
