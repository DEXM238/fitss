<?php
/* @var $this MuscleController */
/* @var $model Muscle */

$this->breadcrumbs=array(
	'Muscles'=>array('index'),
	$model->title=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Muscle', 'url'=>array('index')),
	array('label'=>'Create Muscle', 'url'=>array('create')),
	array('label'=>'View Muscle', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Muscle', 'url'=>array('admin')),
);
?>

<h1>Update Muscle <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>