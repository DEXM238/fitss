<?php
/* @var $this MuscleController */
/* @var $model Muscle */

$this->breadcrumbs=array(
	'Muscles'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Muscle', 'url'=>array('index')),
	array('label'=>'Manage Muscle', 'url'=>array('admin')),
);
?>

<h1>Create Muscle</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>