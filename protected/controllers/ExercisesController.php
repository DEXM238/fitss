<?php
/**
 * Created by JetBrains PhpStorm.
 * User: dimon
 * Date: 08.09.13
 * Time: 0:14
 * To change this template use File | Settings | File Templates.
 */

class ExercisesController extends CController {

    function actionIndex(){

        $criteria = new CDbCriteria;
        $criteria->select = 'id, title';

        $muscles = Muscle::model()->findAll($criteria);

        $this->render('index', array('muscles' => $muscles));
    }

    function actionMuscle($muscle_id){

        $exercises = Exercise::model()->findAllByAttributes(array('muscle_id' => $muscle_id));

        $this->render('exercises_list', array('exercises' => $exercises));
    }
}