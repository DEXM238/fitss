<?php
/**
 * Created by JetBrains PhpStorm.
 * User: dimon
 * Date: 15.09.13
 * Time: 12:44
 * To change this template use File | Settings | File Templates.
 */

class ApiController extends Controller{
    Const APPLICATION_ID = 'ASCCPE';
    Const JSON_CONTENT_TYPE = 'application/json';

    private $format = 'json';

    public function filters(){
        return array();
    }

    //готово
    public function actionList($model){

        switch($model){
            case 'equipment':
                $models = Equipment::model()->findAll();
                break;
            default:
                // Model not implemented error
                $this->_sendResponse(405, sprintf(
                    'Error: Mode <b>list</b> is not implemented for model <b>%s</b>', $model) );
        }

        if(empty($models)){
            $this->_sendResponse(204);
        } else {
            $rows = array();
            foreach($models as $model){
                $rows[] = $model->attributes;
            }
            $this->_sendResponse(200, CJSON::encode($rows), self::JSON_CONTENT_TYPE);
        }
    }

    //готово
    public function actionView($model, $id){

        if(!isset($id)){
            $this->_sendResponse(400, 'Error: Parameter <b>id</b> is missing');
        }

        switch($model){
            case 'equipment':
                $model = Equipment::model()->findByPk($id);
                break;
            default:
                // Model not implemented error
                $this->_sendResponse(405, sprintf(
                    'Error: Mode <b>view</b> is not implemented for model <b>%s</b>', $model) );
        }

        if(is_null($model)){
            $this->_sendResponse(404, 'No items found with id '.$id);
        } else {
            $this->_sendResponse(200, CJSON::encode($model), self::JSON_CONTENT_TYPE);
        }
    }

    public function actionCreate($model){

        switch($model){
            case 'equipment':
                $model = new Equipment();
                break;
            default:
                $this->_sendResponse(405,
                    sprintf('Mode <b>create</b> is not implemented for model <b>%s</b>', $model) );
        }

        foreach($_POST as $var=>$value){
            if($model->hasAttribute($var)){
                $model->$var = $value;
            } else {
                $this->_sendResponse(400,
                    sprintf('Parameter <b>%s</b> is not allowed for model <b>%s</b>', $var, $model) );
            }
        }

        if($model->save()){
            $this->_sendResponse(201, CJSON::encode($model), self::JSON_CONTENT_TYPE,
                array('Location: ' . Yii::app()->getBaseUrl(true) . Yii::app()->request->getUrl() . '/' . $model->id));
        } else {
            // Errors occurred
            $message = "<h1>Error</h1>";
            $message .= sprintf("Couldn't create model <b>%s</b>", $_GET['model']);
            $message .= "<ul>";
            foreach($model->errors as $attribute=>$attr_errors) {
                $message .= "<li>Attribute: $attribute</li>";
                $message .= "<ul>";
                foreach($attr_errors as $attr_error)
                    $message .= "<li>$attr_error</li>";
                $message .= "</ul>";
            }
            $message .= "</ul>";
            $this->_sendResponse(500, $message);
        }
    }

    public function actionUpdate(){

    }

    public function actionDelete(){

    }

    private function _sendResponse($status_code = 200, $body = '', $content_type = 'text/html', $headers = array()){

        //set up headers
        $status_header = 'HTTP/1.1 '.$status_code.' '.$this->_getStatusCodeMessage($status_code);
        header($status_header);
        header('Content-type: '.$content_type);
        if(!empty($headers)){
            foreach($headers as $header){
                header($header);
            }
        }

        //set up body
        if($body != ''){
            echo $body;
        } else {
            $message = '';

            switch($status_code){
                case 400:
                    $message = '';
                    break;
                case 401:
                    $message = 'You must be authorized to view this page.';
                    break;
                case 404:
                    $message = 'The requested URL ' . $_SERVER['REQUEST_URI'] . ' was not found.';
                    break;
                case 405:
                    $message = 'The requested method is not implemented for current resource';
                    break;
                case 500:
                    $message = 'The server encountered an error processing your request.';
                    break;
                case 501:
                    $message = 'The requested method is not implemented.';
                    break;
            }

            $signature = ($_SERVER['SERVER_SIGNATURE'] == '') ? $_SERVER['SERVER_SOFTWARE'].'
                Server at '.$_SERVER['SERVER_NAME'].' Port '.$_SERVER['SERVER_PORT'] :
                $_SERVER['SERVER_SIGNATURE'];

            $body = '
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset="utf-8">
    <title>' . $status_code . ' ' . $this->_getStatusCodeMessage($status_code) . '</title>
</head>
<body>
    <h1>' . $this->_getStatusCodeMessage($status_code) . '</h1>
    <p>' . $message . '</p>
    <hr />
    <address>' . $signature . '</address>
</body>
</html>';

            echo $body;
        }

        Yii::app()->end();
    }

    private function _getStatusCodeMessage($status_code)
    {
        // these could be stored in a .ini file and loaded
        // via parse_ini_file()... however, this will suffice
        // for an example
        $codes = Array(
            200 => 'OK',
            400 => 'Bad Request',
            401 => 'Unauthorized',
            402 => 'Payment Required',
            403 => 'Forbidden',
            404 => 'Not Found',
            405 => 'Method Not Allowed ',
            500 => 'Internal Server Error',
            501 => 'Not Implemented',
        );
        return (isset($codes[$status_code])) ? $codes[$status_code] : '';
    }

}