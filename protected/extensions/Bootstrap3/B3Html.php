<?php
/**
 * Created by JetBrains PhpStorm.
 * User: dimon
 * Date: 01.09.13
 * Time: 14:52
 * To change this template use File | Settings | File Templates.
 */

class B3Html extends CHtml {

    const BTN_DEFAULT = 'btn btn-default';

    public static function submitButton($label='submit',$htmlOptions=array())
    {
        $htmlOptions['type']='submit';
        $htmlOptions = WebHelper::addClass(self::BTN_DEFAULT, $htmlOptions);
        return self::button($label,$htmlOptions);
    }
}