<?php
/**
 * Created by JetBrains PhpStorm.
 * User: dimon
 * Date: 01.09.13
 * Time: 12:27
 * To change this template use File | Settings | File Templates.
 */

Yii::import('CActiveForm');

class B3ActiveForm extends CActiveForm {

    const FORM_CONTROL = 'form-control';

    public function textField($model,$attribute,$htmlOptions=array()) {
        $htmlOptions = WebHelper::addClass(self::FORM_CONTROL, $htmlOptions);
        return parent::textField($model, $attribute, $htmlOptions);
    }

    public function textArea($model,$attribute,$htmlOptions=array()) {
        $htmlOptions = WebHelper::addClass(self::FORM_CONTROL, $htmlOptions);
        return parent::textArea($model, $attribute, $htmlOptions);
    }

    public function dropDownList($model,$attribute,$data,$htmlOptions=array()) {
        $htmlOptions = WebHelper::addClass(self::FORM_CONTROL, $htmlOptions);
        return parent::dropDownList($model,$attribute,$data,$htmlOptions);
    }
}